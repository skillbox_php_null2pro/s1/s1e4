<?
require_once 'core/router.php'; //код роутера
require_once 'logins.php';
require_once 'passwords.php';
require_once 'template/menu/menu.php'; //регистрируем компонент меню
require_once 'core/helpers.php';


//дефолтный логин
$defLogin = 'admin';
$defPassword = 'admin';

$isAuthorized = false;
$isFail = false;

if (isset($_GET['logout'])){
    setcookie('auth', false, time()-3600*24*30);
    header('Location: /');
}

if (isset($_COOKIE['auth'])){
    $isAuthorized = true;
}

//Поля авторизации заполнены?
$isAuthorizable = !empty($_POST['login']) && !empty($_POST['password']);

//Показываем форму авторизации?
$isShowAuthForm = !$isAuthorized && !empty($_GET['login']) && $_GET['login'] === 'yes';

//Авторизация
if (!$isAuthorized && $isAuthorizable) {
    //Зачищаем ввод
    $login = trim(htmlspecialchars($_POST['login']));
    $password = trim(htmlspecialchars($_POST['password']));

    if ($login === $defLogin && $password === $defPassword) {
        $isAuthorized = true;
    } elseif (in_array($login, $arLogins, true)) {
        $idx = array_search($login, $arLogins, true);

        if ($idx !== false) {
            if ($password === $arPasswords[$idx]) {
                $isAuthorized = true;
            }
        }
    }

    //устанавливаем куку
    if ($isAuthorized) {
        setcookie('auth', true);
    }

    if (!$isAuthorized) {
        $isFail = true;
    }
}

if (!$isAuthorized && !$isFail){
    echo '<p><a href="/?login=yes" style="color: yellow">login</a></p>';
}
if ($isAuthorized){
    echo '<p><a href="/?logout" style="color: yellow">logout</a></p>';
}
