<ul>
    <?foreach ($MENU as $item):?>
        <li>
            <a href="<?=$item['path']?>" style="text-decoration: <?= ($item['selected']) ? 'underline' : 'none' ?>;">
                <?=titleCrop($item['title'])?>
            </a>
        </li>
    <?endforeach?>
</ul>