<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/include/core/helpers.php';

/**Модуль, выводящий меню
 *
 * @param string $template - шаблон меню
 * @param string $sort     - порядок сортировки пунктов
 */
function menu($template, $sort = 'asc')
{
    global $isAuthorized;
    if (!$isAuthorized) {
        return;
    }

    if (!$template) {
        return;
    }

    $menu = getMenu();
    arraySort($menu, 'sort', $sort);

    $path = getSectionPath();
    foreach ($menu as &$section) {
        $section['selected'] = ($section['path'] == $path);
    }


    includeTemplate($template, __DIR__, $menu);
}
