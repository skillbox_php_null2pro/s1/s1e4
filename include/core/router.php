<?
require_once 'helpers.php';

//Выбрасываем 404, если запрос не подошел под шаблоны
if (is404()) {
    header("HTTP/1.0 404 Not Found");
    ?><p><?= $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] ?></p><p>Страница не найдена</p><?
    die();
}
