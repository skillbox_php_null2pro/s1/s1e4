<?
/**Возвращает глобальное меню
 *
 * @return array $MENU
 */
function getMenu()
{
    return require $_SERVER['DOCUMENT_ROOT'] . '/main_menu.php';
}

/**Возвращает true, если путь не существует
 *
 * @return bool
 */
function is404()
{
    $isRoot = preg_match("/^\/$|^\/[?][^\/]*/", $_SERVER['REQUEST_URI']);// root
    $isIndex = preg_match("/^\/index.php$|^\/index.php[?][^\/]*/", $_SERVER['REQUEST_URI']); // /index.php
    $isSections = (getSectionPath()) ? true : false;

    if ($isRoot || $isIndex || $isSections) {
        return false;
    }

    return true;
}

/**Возвращает false или путь к секции
 *
 * @return false|string путь к секции в корректном формате
 */
function getSectionPath()
{

    preg_match("/\/route\/\w*(\/?)/", $_SERVER['REQUEST_URI'], $matches); // /route

    if (!$matches) {
        return false;
    }

    if (!$matches[1]) {
        $matches[0] .= '/';
    }

    return $matches[0];
}

/**Получает название раздела
 *
 * @return null|string
 */
function sectionName()
{
    $menu = getMenu();
    $path = getSectionPath();

    foreach ($menu as $section) {
        $title = ($section['path'] == $path) ? $section['title'] : null;
    }

    return $title;
}

/**Обрезает заголовок
 *
 * @param      $title
 * @param bool $return - вернуть новый заголовок, или изменить по ссылке
 *
 * @return string
 */
function titleCrop($title)
{
    if (strlen($title) > 15) {
        $title = substr($title, 0, 12) . '...';
    }

    return $title;
}

/**Сортирует массив по ключу sort
 *
 * @param $array
 * @param $order
 */
function arraySort(&$array, $key = 'sort', $order = 'asc')
{
    uasort(
        $array,
        function($a, $b) use ($key, $order) {
            return ($order == 'desc') ? $b[$key] <=> $a[$key] : $a[$key] <=> $b[$key];
        }
    );
}

/**Подключает шаблон по пути
 *
 * @param $template
 * @param $dir
 */
function includeTemplate($template, $dir, $MENU)
{
    $template = $dir . '/template/' . $template . '.php';
    if (file_exists($template)) {
        require_once $template;
    }
}
