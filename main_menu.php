<?
$MENU = [
    [
        'title' => 'Section 5',
        'path'  => '/route/section5/',
        'sort'  => 500,
    ],
    [
        'title' => 'Section 2',
        'path'  => '/route/section2/',
        'sort'  => 200,
    ],
    [
        'title' => 'Section 1 very long title',
        'path'  => '/route/section1/',
        'sort'  => 100,
    ],
    [
        'title' => 'Section 4',
        'path'  => '/route/section4/',
        'sort'  => 400,
    ],
    [
        'title' => 'Section 3',
        'path'  => '/route/section3/',
        'sort'  => 300,
    ],
];
return $MENU;
